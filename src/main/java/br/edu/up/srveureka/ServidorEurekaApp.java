package br.edu.up.srveureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class ServidorEurekaApp {

	public static void main(String[] args) {
		SpringApplication.run(ServidorEurekaApp.class, args);
	}

}
